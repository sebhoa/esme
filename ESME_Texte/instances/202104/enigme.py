import json
import time
import e2

class Enigme:

    def __init__(self, eid, question, solution):
        self.eid = eid
        self.question = question
        self.solution = solution

    def enonce(self):
        if self.eid == 2:
            question = e2.BOB
        else:
            question = self.question
        for para in question:
            for c in para:
                print(c, end='', flush=True)
                time.sleep(0.01)
            self.continuer()

    def continuer(self):
        print()
        input('Press return to continue...')
        print()

    def reponse(self):
        rep = input('? ')
        while rep == '' or rep not in self.solution:
            rep = input('? ')

class Game:

    def __init__(self, dossier):
        self.dossier = dossier
        self.nb = 0
        self.enigmes = []

    
    def settings(self):
        with open(f'{self.dossier}/intro.txt') as intro:
            self.nb = int(intro.readline().strip())
        for eid in range(self.nb):
            filename = f'{self.dossier}/e{eid}.txt'
            with open(filename, 'r', encoding='utf-8') as txtfile:
                ligne = txtfile.readline()
                paragraphes = []
                question = ''
                while ligne:
                    if ligne == '--\n':
                        ligne = txtfile.readline().strip()
                        self.enigmes.append(Enigme(eid, paragraphes, ligne))
                    elif ligne == '\n':
                        paragraphes.append(question)
                        question = ''
                    else:
                        question += ligne
                    ligne = txtfile.readline()


    def loop(self):
        for i in range(self.nb):
            enigme = self.enigmes[i]
            enigme.enonce()
            enigme.reponse()

    def start(self):
        self.settings()
        self.loop()

paques = Game('202104')
# paques.settings()
# for q in paques.enigmes:
#     print(q.question)
#     print(q.solution)
paques.start()    



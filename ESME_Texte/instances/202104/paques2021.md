Pâques oblige, notre chasse commence par un _easter egg_...

Sont numérotées uniquement les énigmes qui ont leur énoncé sur le mac.

- **Enigme 0** 

    > Easter egg dans Start Wars VII, le réveil de la Force.<br> 
    > Il concerne Finn... trouveras tu la réf ? <br>
    > Quand tu l'auras, utilise le téléphone de papautiste...
    
    _Solution_
    
    FN-2187 = ID de la cellule de Leia dans Star Wars I **2187** dévérouille mon tél contenant la 2e énigme : la photo du coffre de Yvan

- **Enigme 0b** Une photo montrant une serrure... il faut retrouver le coffre qui recèle l'énigme suivante :

- **Enigme 0c** 

    > Aaaah l'aventure... les héros, les aventuriers... Voyons ces lettres... elles me rappellent quelque chose ou quelqu'un ? Mais qui ? 
    
    **Réponse 0** : Simba

- **Enigme 1**

    > `59°19'40"N`  `18°05'29"E` <br>
    > L'année de sa résurrection est la clé. 
    
    Les coordonnées sont celles du musée Vasa en Suède qui rend hommage au vaisseau du même nom coulé en 1628 et renfloué en 
    
    **Réponse 1** : 1961

    Donner cette année à la machine qui révèle Bob -> la clé et le compte insta une photo du coffre qui contient l'énigme 2

- **Enigme 2**

    La carte dans le coffre et les indications permettent de trouver Shizuoka la préfecture de My Hero Academia et de son héro
   
   **Réponse 2** : Deku 
   
   à entrer sur le mac pour débloquer l'énoncé suivant = lien youtube

6. Extrait musical pour trouver 
    > Samourai Champloo

    Réponse à donner à Madame Marile

7. On cherche une saison (indication à ne donner que si elles galèrent)... N188 H175 X81 D27 S1 G25 B128. Les coordonnées donnent BOB23-1 et le titre de la vidéo de l'épisode 1 saison 23 donne
    > hiver

8. Sur le mac l'énoncé dit : "Sur le bureau il y a un dossier mystere contanant un fichier... ouvrez-le
    https://www.youtube.com/watch?v=sZms0wGtWAU (dalmatien)
    https://www.youtube.com/watch?v=o_77_AmD14A 12'26" St-Hubert
    https://www.youtube.com/watch?v=nyN042bykkM 0'30" Dobermann

    On cherche 3 races de chien : 
    
    > Dalmatien, St-Hubert et Dobermann


9. - domaine : l'histoire
   - à découvrir : une date 

10. - domaine : la chanson
   - à découvrir : un mot dans les paroles ?

10. - domaine : la géographie
    - à découvrir : un pays
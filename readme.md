# EScape gaME 

ESME est un projet de générateur d'_escape games_ basiques en langage Python. Il existe deux types d'_escape game_ :

1. Le _Labyrinthe_ basé sur le module `turtle`
2. La _Suite d'énigmes_ basée sur un mode texte et l'utilisation de la console.

## Le _Labyrinthe_

A la base il s'agit du projet proposé aux apprenants du MOOC Apprendre à coder avec Python à partir de la session 4 (sept. 2020 à mai 2021). Ce projet a été légèrement modifié mais reste très proche dans la conception :

- un _héro_ représenté par un petit artefact dans une fenêtre graphique du module `turtle` de Python doit trouver son chemin pour atteindre la sortie de ce qui peut être vu comme un lieu constitué d'une succession de couloirs et de salles séparés par des portes. 
- chaque porte est verrouillée et demande une clé sous la forme de la réponse à une question pour être ouverte
- le lieu est parsemé d'indices que le _héro_ peut ramasser en passant sur les cases concernées

Voici un exemple de la version originale (proposée dans le mooc) :

![exemple chateau](video_jeu.gif)

On trouvera [ici une vidéo de la version tranformée](https://youtu.be/C3LWb6pc8Kk).

La version transformée inclus :

- la possibilité de lancer un timer
- le mode nuit : les cases non visitées sont plongées dans le noir
- la carte peut être aussi grande qu'on veut : un recentrage en niveau du héro est effectué quand on risque de sortir de la zone

### A faire 

Il faudrait transformer l'interface pour utiliser `pygame` et avoir un _vrai_ jeu.

## La _suite d'énigmes_

Contrairement au _labyrinthe_, il s'agit d'un jeu linéaire : chaque énigme demande d'être résolue pour passer à la suivante. Certaines réponses sont des _clés_ pour interagir avec le monde réel et donner accès à une énigme hors du système. Mais à tout moment on revient vers le système qui doit valider la progression.

### A faire

Jusque là les suites d'énigmes étaient _bricolées_ au cas par cas. Il s'agit de faire un système basé sur une structure et des fichiers de configuration. Cela permettrait d'instancier facilement une suite d'énigmes.

Si je deviens plus faible,<br>
Les corps peuvent s’envoler.

Si je devient plus forte,<br>
Les corps sont déformés.
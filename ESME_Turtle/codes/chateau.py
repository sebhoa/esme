"""
chateau2.py

Ajout par rapport à la version 1 : le héros avance dans le noir. Il ne voit que quelques cases autours de lui
"""

import argparse
import turtle

from constantes import *

# ----- UTILITAIRES 
# -----


def aller(tortue, x, y):
    tortue.up()
    tortue.goto(x, y)
    tortue.down()

def mark(tortue, x, y, couleur):
    """Dessine un carré dont le sommet inférieur gauche est en x, y, de couleur intérieur couleur, de couleur extérieure la couleur constante définie par COULEUR_EXTERIEURE et de dimension ECH"""
    aller(tortue, x, y)
    tortue.color(COULEUR_EXTERIEUR, couleur)
    tortue.begin_fill()
    for _ in range(4):
        tortue.fd(ECH)
        tortue.left(90)
    tortue.end_fill()




# ----- LE CHATEAU et SES CELLULES
# -----

class Cell:
    """Une  cellule générique du château, avec une coordonnée
    cela peut être l'ENTREE, la SORTIE ou juste une cellule VIDE
    version 2 : une cell est visible ou pas
    """

    def __init__(self, type_cell, x, y, chateau):
        self.x = x
        self.y = y
        self.type = type_cell
        self.__visible = False
        self.chateau = chateau

    @property
    def visible(self):
        return self.__visible

    @visible.setter
    def visible(self, on_off):
        self.__visible = on_off 

    def sortie(self):
        return self.type == SORTIE

    def accessible(self):
        return self.type != MUR

    def seen(self):
        self.change_type(SEEN)

    def change_type(self, type_cell):
        """Change la nature d'une cellule et informe le chateau qu'il
        faut mettre à jour la vue"""
        self.type = type_cell
        self.chateau.update_view(self)

    def interaction(self):
        pass

    def setup(self, *args):
        pass


class Porte(Cell):

    def __init__(self, *args):
        self.question = str()
        self.solution = str()
        super(Porte, self).__init__(PORTE, *args)

    def setup(self, *args):
        self.question, self.solution = args

    def verrouillee(self):
        return self.type == PORTE

    def accessible(self):
        return not self.verrouillee()

    def deverrouille(self):
        self.change_type(VIDE)

    def bonne_reponse(self, reponse):
        return reponse is not None and reponse.lower() == self.solution.lower()

    # Le contrôleur
    #
    def interaction(self):
        """Contrôleur de la porte
        Si la porte est verrouillée, la porte demande au chateau de poser la question au joueur. 
        La porte recupere la reponse fournie et si c'est une bonne réponse, se déverrouille sinon reste verrouillée. A chaque étape, le chateau demande au mdj de faire des annonces appropriées au joueur
        """ 
        if self.verrouillee():
            self.chateau.annonce(MSG_PORTE_VERROUILLEE)
            reponse = self.chateau.ask(self.question)
            if self.bonne_reponse(reponse):
                self.deverrouille()
                self.chateau.annonce(MSG_PORTE_DEVERROUILLEE)
            else:
                self.chateau.annonce(MSG_MAUVAISE_REPONSE)


class Objet(Cell):

    def __init__(self, *args):
        self.nom = str()
        super(Objet, self).__init__(OBJET, *args)

    def setup(self, *args):
        self.nom = args[0]

    def accessible(self):
        return self.type != OBJET

    def interaction(self):
        self.chateau.donner(self.nom)
        self.change_type(VIDE)    


# ----- 
# ----- LE CHATEAU

class Chateau:

    def __init__(self, mdj):
        self.map = list() # matrice de Cell
        self.w = 0
        self.h = 0
        self.mode_nuit = False
        self.mdj = mdj
        self.centre = 0, 0
        self.view = turtle.Turtle()    # pour la visualisation du chateau

    def setup(self, f_plan, f_obj, f_portes, mode_nuit):
        self.mode_nuit = mode_nuit
        self.initialiser_plan(f_plan)
        self.init_portes_et_objets(f_portes)
        self.init_portes_et_objets(f_obj)

        # Réglages de la tortue
        self.view.ht()
        self.view.screen.tracer(0)


    def translate(self, x, y):
        xc, yc = self.centre
        return (x - xc) * ECH, (yc - y) * ECH


    def initialiser_plan(self, fichier, delimiter=DELIMITER):
        with open(fichier, 'r') as f_in:
            for y, ligne in enumerate(f_in):
                self.map.append([])
                for x, str_value in enumerate(ligne.strip().split(delimiter)):
                    type_cell = int(str_value)
                    if type_cell in (VIDE, SORTIE, ENTREE, MUR):
                        if type_cell == ENTREE:
                            self.centre = x, y
                        self.map[-1].append(Cell(type_cell, x, y, self))
                    elif type_cell == PORTE:
                        self.map[-1].append(Porte(x, y, self))
                    elif type_cell == OBJET:
                        self.map[-1].append(Objet(x, y, self))
        self.w, self.h = len(self.map[0]), len(self.map)


    def recentrer(self, x, y):
        self.centre = x, y

    def init_portes_et_objets(self, fichier, delimiter=DELIMITER):
        with open(fichier, 'r', encoding='utf-8') as f_in:
            for ligne in f_in:
                y, x, *args = ligne.strip().split(delimiter)
                self.cell(int(x), int(y)).setup(*args)

    def inside(self, x, y):
        return 0 <= x < self.w and 0 <= y < self.h

    def view(self):
        return self.view

    def cell(self, x, y):
        return self.map[y][x]

    def around(self, x, y):
        return {self.cell(x+dx, y+dy) 
                for dx in range(-DISTANCE_MAX+1,DISTANCE_MAX)
                for dy in range(-DISTANCE_MAX+1, DISTANCE_MAX)
                if self.inside(x+dx, y+dy)}

    def visible(self, x, y, on_off):
            for cell in self.around(x, y):
                cell.visible = on_off
                self.update_view(cell)

    def inbox(self, xv, yv):
        return X_MIN <= xv <= X_MAX and Y_MIN <= yv <= Y_MAX

    def init_view(self):
        """Parcourt les cellule de la map et met à jour la vue"""
        for ligne in self.map:
            for cell in ligne:
                self.update_view(cell)
        self.mdj.update()

    def clear(self):
        self.view.clear()

    def update_view(self, cell):
        """Pour mettre à jour la vue, d'une cellule"""
        xv, yv = self.translate(cell.x, cell.y)
        if self.inbox(xv, yv):
            if cell.visible or not self.mode_nuit:
                couleur = COULEURS[cell.type]
            else:
                couleur = COULEUR_INVISIBLE
            mark(self.view, xv, yv, couleur)
    
    def inside(self, x, y):
        return 0 <= x < self.w and 0 <= y < self.h

    def annonce(self, msg):
        self.mdj.annonce(msg)
    
    def donner(self, nom):
        self.mdj.annonce(f'{MSG_INDICE_TROUVE} : {nom}')

    def ask(self, question):
        return self.mdj.ask(question)

    def active_ecoute(self):
        self.mdj.active_ecoute()



# ----- 
# ----- LE HEROS

class Heros:

    def __init__(self, chateau, mdj):
        self.x = None
        self.y = None
        # self.ech = 0
        self.chateau = chateau
        self.mdj = mdj
        self.view = turtle.Turtle()

    def __str__(self):
        return f'heros en {self.x},{self.y}'

    # -- Le modèle

    def setup(self):
        self.x, self.y = self.chateau.centre
        self.chateau.visible(self.x, self.y, on_off=True)
        
        # Réglages de la tortue
        self.view.color(COULEUR_PERSONNAGE)
        self.view.shape('circle')
        self.view.shapesize(0.6)
        self.view.up()

    def goto(self, dx, dy):
        self.x += dx
        self.y += dy

    def found_exit(self):
        return self.chateau.cell(self.x, self.y).sortie()

    # -- La vue du joueur
    
    def limite(self, xv, yv):
        return X_MAX - abs(xv) < 2*ECH or abs(Y_MAX - yv) < 2*ECH or abs(Y_MIN - yv) < 2*ECH

    def update_view(self):
        xv, yv = self.chateau.translate(self.x, self.y)
        aller(self.view, xv + ECH // 2, yv + ECH // 2)
        if self.limite(xv, yv):
            self.chateau.recentrer(self.x, self.y)
            xv, yv = self.chateau.translate(self.x, self.y)
            self.chateau.clear()
            aller(self.view, xv + ECH // 2, yv + ECH // 2)
            self.chateau.init_view()
        self.mdj.update()


    # -- Le contrôleur du joueur
    
    def move_up(self):
        self.avancer(UP)

    def move_down(self):
        self.avancer(DOWN)

    def move_left(self):
        self.avancer(LEFT)

    def move_right(self):
        self.avancer(RIGHT)

    def avancer(self, direction):
        dx, dy = direction
        origine = self.chateau.cell(self.x, self.y)
        if self.chateau.inside(self.x + dx, self.y + dy):
            destination = self.chateau.cell(self.x + dx, self.y + dy)
            if destination.accessible():
                self.chateau.visible(self.x + dx, self.y + dy, on_off=True)
                origine.seen()
                self.goto(dx, dy)
                self.update_view()
                if self.found_exit():
                    self.mdj.end()
            else:
                destination.interaction()



class Timer:

    def __init__(self, h, m, s):
        self.h = h
        self.m = m
        self.s = s
        self.id = None
        self.view = turtle.Turtle()

    def setup(self):
        self.view.ht()
        self.view.up()
        aller(self.view, *POINT_AFFICHAGE_TIMER)
    
    def update_view(self):
        self.view.clear()
        self.view.write(f'{self.h:02}:{self.m:02}:{self.s:02}', align='right', font=FONT)
    
    def decompte(self):
        h, m, s = self.h, self.m, self.s
        s -= 1
        if s < 0:
            s = 59
            m -= 1
            if m < 0:
                m = 59
                h -= 1
        self.h, self.m, self.s = h, m, s
        self.update_view()
        self.id = self.view.screen.getcanvas().after(1000, self.decompte)
    
    def on(self):
        self.decompte()

    def off(self):
        self.view.screen.getcanvas().after_cancel(self.id)

# ----- 
# ----- LE MAITRE DE JEU

class MaitreDeJeu:

    def __init__(self):
        self.chateau = Chateau(self)
        self.heros = Heros(self.chateau, self)
        self.timer = None
        self.controleur = turtle.Screen()   # gère les évévements
        self.msg_view = turtle.Turtle()     # les messages à l'utilisateur


    def setup(self):
        # Traitement des options
        #
        parser = argparse.ArgumentParser()
        parser.add_argument('-s', '--set', help='numéro du set de fichiers', type=int)
        parser.add_argument('-n', '--nuit', help='active le mode nuit', default=False, action='store_true')
        parser.add_argument('-t', '--timer', help='active le timer hh:mm:ss')

        args = parser.parse_args()
        set_fichier = int(args.set) if args.set else 1
        h,m,s = tuple(int(e) for e in args.timer.split(':'))
        self.timer = Timer(h,m,s) 
        fichier_plan = f'fichiers/plan_chateau_{set_fichier}.csv'
        fichier_objets = f'fichiers/dico_objets_{set_fichier}.csv'
        fichier_portes = f'fichiers/dico_portes_{set_fichier}.csv'
        
        # Réglage de la tortue des messages et du timer
        self.msg_view.ht()
        aller(self.msg_view, *POINT_AFFICHAGE_ANNONCES)
        self.timer.setup()

        # Setup du chateau et du heros
        self.chateau.setup(fichier_plan, fichier_objets, fichier_portes, args.nuit)
        self.heros.setup()


    def active_ecoute(self):
        self.controleur.listen()

    def ask(self, question):
        reponse = self.controleur.textinput('Question', question)
        self.active_ecoute()
        return reponse

    def annonce(self, msg):
        self.msg_view.clear()
        self.msg_view.write(msg, font=FONT)        
    
    # def note_inventaire(self, msg):
    #     self.sac_view.write(msg, font=FONT)
    #     self.sac_view.up()
    #     self.sac_view.fd(HAUTEUR_TEXTE)


    def unbind(self):
        for key in (KEY_UP, KEY_DOWN, KEY_LEFT, KEY_RIGHT):
            self.controleur.onkey(None, key)

    def bind(self):
        self.controleur.onkey(self.heros.move_up, KEY_UP)
        self.controleur.onkey(self.heros.move_down, KEY_DOWN)
        self.controleur.onkey(self.heros.move_left, KEY_LEFT)
        self.controleur.onkey(self.heros.move_right, KEY_RIGHT)
    
    def timer_on(self):
        # self.controleur.ontimer(self.timer.decompte, 1000)
        self.timer.on()
    
    def timer_off(self):
        self.timer.off()

    def update(self):
        self.controleur.update()

    def end(self):
        self.timer_off()
        self.annonce(MSG_VICTOIRE)
        self.unbind()
        
    def start(self):
        self.annonce(MSG_DEBUT)
        self.chateau.init_view()
        self.heros.update_view()
        self.timer.update_view()
        self.bind()
        self.timer_on()
        self.controleur.listen()
        self.controleur.mainloop()


# ----- 
# ----- PROGRAMME PRINCIPAL

test = MaitreDeJeu()
test.setup()
test.start()


